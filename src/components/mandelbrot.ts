
import { LitElement, html } from 'lit-element';
import { customElement } from 'lit-element';


// generate mandel brot 
@customElement("my-mandelbrot")
export class CanvasMandelBrot extends LitElement {

	public readonly id: string = '1';
	public width: number = 200;
	public height: number = 400;

	// get tag attributes
	static get properties() {
		return {
			width: { type: Number },
			height: { type: Number },
			id: { type: String }
		}
	}


	// constructor() {
	// 	super();
	// 	this.width = width;
	// 	this.height = height;
	// 	this.id = id;

	// }

	static get_n_diverged(x0: number, y0: number, max_iter: number) {
		// 発散判定を行う
		let xn = 0;
		let yn = 0;
		let xnext;
		let ynext;

		for (let i = 1; i < max_iter; i++) {

			xnext = xn * xn - yn * yn + x0;
			ynext = 2 * xn * yn + y0;
			xn = xnext;
			yn = ynext;

			if (((xn * xn + yn * yn)) > 4) {
				return i;
			}

		}

		return max_iter;
	}

	generate_mandelbrot(
		canvas_w: number,
		canvas_h: number,
		x_min: number,
		x_max: number,
		y_min: number,
		y_max: number,
		max_iter: number) {

		let data = [];
		for (let i = 0; i < canvas_h; i++) {

			let y = y_min + (y_max - y_min) * i / canvas_h;

			for (let j = 0; j < canvas_w; j++) {
				let x = x_min + (x_max - x_min) * j / canvas_w;

				let iter_index = CanvasMandelBrot.get_n_diverged(x, y, max_iter);
				// eight color
				let v = iter_index % 8 * 32;
				data.push(v);
				data.push(v);
				data.push(v);
				data.push(255);


			}
		}

		return data;
	}

	click() {
		console.log("lick")
	}
	draw(datapoints: Uint8ClampedArray, canvas_w: number, canvas_h: number) {
		if(this.shadowRoot){
			const canvas = this.shadowRoot.querySelector("canvas")!as HTMLCanvasElement;
			const context = canvas.getContext("2d");
			let img = new ImageData(datapoints, canvas_w, canvas_h);
			if (context !== null) {
				context.putImageData(img, 0, 0);
			}
		}
		

	}

	render() {
		return html`
		<div class="my-mandel-brot-wrapper" id=${this.id}>
			<h3>Mandelbrot Canvas</h3>
			<button @click=${() => {
					this.draw(new
						Uint8ClampedArray(this.generate_mandelbrot(this.width, this.height, -1, 1.5, -1, 1, 64)), this.width,
						this.height)
					console.log("lick")
				}} type="button">show</button>
			<canvas width=${this.width} height=${this.height}>
		
			</canvas>
		</div>
		

		`
	}
}

// customElements.define('my-mandelbrot', CanvasMandelBrot);
// 